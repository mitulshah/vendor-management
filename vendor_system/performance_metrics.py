from django.db.models import Avg, F, ExpressionWrapper, DurationField
from .models import PurchaseOrder, Vendor


def calculate_performance_metrics(vendor):
    orders = PurchaseOrder.objects.filter(vendor=vendor, status="completed")
    total_orders = orders.count()

    # On-Time Delivery Rate
    on_time_orders = orders.filter(delivery_date__lte=F("delivery_date")).count()
    vendor.on_time_delivery_rate = (
        on_time_orders / total_orders if total_orders else 0.0
    )

    # Quality Rating Average
    quality_ratings = orders.exclude(quality_rating=None).aggregate(
        Avg("quality_rating")
    )["quality_rating__avg"]
    vendor.quality_rating_avg = quality_ratings if quality_ratings else 0.0

    # Average Response Time
    response_times = (
        orders.exclude(acknowledgment_date=None)
        .annotate(
            response_time=ExpressionWrapper(
                F("acknowledgment_date") - F("issue_date"), output_field=DurationField()
            )
        )
        .aggregate(Avg("response_time"))["response_time__avg"]
    )
    vendor.average_response_time = (
        response_times.total_seconds() / total_orders if response_times else 0.0
    )

    # Fulfillment Rate
    successful_orders = orders.filter(quality_rating__gte=3).count()
    vendor.fulfillment_rate = successful_orders / total_orders if total_orders else 0.0

    # Save the updated performance metrics
    vendor.save()
