from django.utils import timezone
from rest_framework import viewsets, permissions
from rest_framework.decorators import action
from rest_framework.decorators import api_view
from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response

from .models import Vendor, PurchaseOrder
from .serializers import VendorSerializer, PurchaseOrderSerializer
from .performance_metrics import calculate_performance_metrics


class VendorViewSet(viewsets.ModelViewSet):
    queryset = Vendor.objects.all()
    serializer_class = VendorSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=["get"])
    def performance(self, request, pk=None):
        vendor = self.get_object()
        performance_metrics = {
            "on_time_delivery_rate": vendor.on_time_delivery_rate,
            "quality_rating_avg": vendor.quality_rating_avg,
            "average_response_time": vendor.average_response_time,
            "fulfillment_rate": vendor.fulfillment_rate,
        }
        return Response(performance_metrics)


class PurchaseOrderViewSet(viewsets.ModelViewSet):
    queryset = PurchaseOrder.objects.all()
    serializer_class = PurchaseOrderSerializer


@swagger_auto_schema(
    method="post",
    operation_summary="Acknowledge a purchase order",
    operation_description="Update the acknowledgment date of a purchase order and recalculate vendor performance metrics",
    responses={200: "Purchase Order acknowledged successfully"},
)
@api_view(["POST"])
def acknowledge_purchase_order(request, po_id):
    try:
        po = PurchaseOrder.objects.get(id=po_id)
        po.acknowledgment_date = timezone.now()
        po.save()
        calculate_performance_metrics(po.vendor)
        return Response(
            {"message": "Purchase Order acknowledged successfully"}, status=200
        )
    except PurchaseOrder.DoesNotExist:
        return Response({"error": "Purchase Order not found"}, status=404)
