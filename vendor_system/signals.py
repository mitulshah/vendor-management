from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from .models import PurchaseOrder, Vendor
from .performance_metrics import calculate_performance_metrics


@receiver(post_save, sender=PurchaseOrder)
def update_vendor_performance_on_save(sender, instance, created, **kwargs):
    calculate_performance_metrics(instance.vendor)


@receiver(post_delete, sender=PurchaseOrder)
def update_vendor_performance_on_delete(sender, instance, **kwargs):
    calculate_performance_metrics(instance.vendor)
