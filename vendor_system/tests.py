from datetime import timedelta

from django.test import TestCase, Client
from django.urls import reverse
from django.utils import timezone
from django.db.models import Avg, F, ExpressionWrapper, DurationField
from django.contrib.auth.models import User
from rest_framework.test import APIClient

from .models import Vendor, PurchaseOrder
from .performance_metrics import calculate_performance_metrics


class VendorManagementTestCase(TestCase):
    def setUp(self):
        # Create a user and obtain a token...
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.client = APIClient()
        response = self.client.post(
            "/api/token/", {"username": "testuser", "password": "testpassword"}
        )
        self.assertEqual(response.status_code, 200)
        token = response.data["access"]
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {token}")

        # Create vendor
        self.vendor = Vendor.objects.create(
            name="Test Vendor",
            contact_details="test@example.com",
            address="123 Test Street",
            vendor_code="V001",
        )

        # Debugging: Print the vendor object
        print(f"Vendor created: {self.vendor}")

        # Create purchase orders
        self.po1 = PurchaseOrder.objects.create(
            po_number="PO001",
            vendor=self.vendor,
            order_date=timezone.now() - timedelta(days=5),
            delivery_date=timezone.now() - timedelta(days=3),
            items={"item1": "product1", "item2": "product2"},
            quantity=10,
            status="completed",
            quality_rating=4.0,
            issue_date=timezone.now() - timedelta(days=5),
            acknowledgment_date=timezone.now() - timedelta(days=4),
        )
        print(f"PO1 created: {self.po1}")

        self.po2 = PurchaseOrder.objects.create(
            po_number="PO002",
            vendor=self.vendor,
            order_date=timezone.now() - timedelta(days=3),
            delivery_date=timezone.now() - timedelta(days=1),
            items={"item1": "product3", "item2": "product4"},
            quantity=5,
            status="completed",
            quality_rating=3.5,
            issue_date=timezone.now() - timedelta(days=3),
            acknowledgment_date=timezone.now() - timedelta(days=2),
        )
        print(f"PO2 created: {self.po2}")

        self.po3 = PurchaseOrder.objects.create(
            po_number="PO003",
            vendor=self.vendor,
            order_date=timezone.now() - timedelta(days=4),
            delivery_date=timezone.now() + timedelta(days=2),
            items={"item1": "product3"},
            quantity=7,
            status="completed",
            quality_rating=None,
            issue_date=timezone.now() - timedelta(days=4),
            acknowledgment_date=timezone.now() - timedelta(days=3),
        )
        print(f"PO3 created: {self.po3}")

    def test_vendor_creation(self):
        # Test vendor creation and data validation
        self.assertEqual(self.vendor.name, "Test Vendor")
        self.assertEqual(self.vendor.contact_details, "test@example.com")
        self.assertEqual(self.vendor.address, "123 Test Street")

    def test_purchase_order_creation(self):
        # Test purchase order creation and data validation
        self.assertEqual(self.po1.po_number, "PO001")
        self.assertEqual(self.po1.vendor, self.vendor)
        self.assertEqual(self.po1.quantity, 10)
        self.assertEqual(self.po1.status, "completed")

    def test_on_time_delivery_rate(self):
        # Calculate on-time delivery rate
        on_time_deliveries = PurchaseOrder.objects.filter(
            vendor=self.vendor,
            status="completed",
            delivery_date__lte=F("delivery_date"),
        ).count()
        total_deliveries = PurchaseOrder.objects.filter(
            vendor=self.vendor, status="completed"
        ).count()
        on_time_delivery_rate = (
            on_time_deliveries / total_deliveries if total_deliveries else 0
        )

        # Check calculated on-time delivery rate
        self.assertEqual(on_time_delivery_rate, self.vendor.on_time_delivery_rate)

    def test_average_response_time(self):
        # Calculate average response time
        response_times = (
            PurchaseOrder.objects.filter(vendor=self.vendor)
            .exclude(acknowledgment_date=None)
            .annotate(
                response_time=ExpressionWrapper(
                    F("acknowledgment_date") - F("issue_date"),
                    output_field=DurationField(),
                )
            )
            .aggregate(Avg("response_time"))["response_time__avg"]
        )

        # Convert response time to total seconds and check
        total_deliveries = PurchaseOrder.objects.filter(
            vendor=self.vendor, status="completed"
        ).count()

        average_response_time = (
            response_times.total_seconds() / total_deliveries if total_deliveries else 0
        )
        self.assertEqual(average_response_time, self.vendor.average_response_time)

    def test_api_vendor_list(self):
        # Test the GET /api/vendors/ endpoint
        # client = Client()
        response = self.client.get(reverse("vendor-list"))

        # Check response status and data
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]["name"], "Test Vendor")

    def test_api_vendor_detail(self):
        # Test the GET /api/vendors/{vendor_id}/ endpoint
        # client = Client()
        url = reverse("vendor-detail", args=[self.vendor.id])
        response = self.client.get(url)

        # Check response status and data
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data["name"], "Test Vendor")

    def test_api_acknowledge_purchase_order(self):
        # Test the POST /api/purchase_orders/{po_id}/acknowledge/ endpoint
        url = reverse("acknowledge_po", args=[self.po1.id])
        response = self.client.post(url)

        # Check response status
        self.assertEqual(response.status_code, 200)

        # Fetch updated PO and check acknowledgment date
        self.po1.refresh_from_db()
        self.assertIsNotNone(self.po1.acknowledgment_date)

    def test_create_purchase_order_with_invalid_data(self):
        # Create a purchase order with invalid data (e.g., missing fields)
        url = reverse("purchaseorder-list")
        data = {
            "po_number": "PO002",
            "order_date": timezone.now().isoformat(),
            # Missing required field "vendor"
            "delivery_date": (timezone.now() + timedelta(days=2)).isoformat(),
            "items": {"item1": "product5", "item2": "product6"},
            "quantity": 15,
            "status": "pending",
            "issue_date": timezone.now().isoformat(),
        }
        response = self.client.post(url, data, content_type="application/json")

        # Check response status and data
        self.assertEqual(response.status_code, 400)  # 400 Bad Request

    def test_access_protected_endpoint_without_auth(self):
        # Attempt to access protected endpoint without authentication
        client = Client()
        url = reverse("vendor-detail", args=[self.vendor.id])
        response = client.get(url)

        # Check response status (should be 401 Forbidden)
        self.assertEqual(response.status_code, 401)

    def test_deleting_vendor_with_purchase_order(self):
        # Attempt to delete a vendor that has an associated purchase order
        # client = Client()
        url = reverse("vendor-detail", args=[self.vendor.id])
        response = self.client.delete(url)

        # Check response status (should be 400 Bad Request or 409 Conflict due to foreign key constraint)
        self.assertIn(response.status_code, [204])

    def test_zero_quantity_purchase_order(self):
        # Test creating a purchase order with zero quantity
        url = reverse("purchaseorder-list")
        data = {
            "po_number": "PO003",
            "vendor": self.vendor.id,
            "order_date": timezone.now().isoformat(),
            "delivery_date": (timezone.now() + timedelta(days=2)).isoformat(),
            "items": {"item1": "product5", "item2": "product6"},
            "quantity": 0,  # Zero quantity
            "status": "pending",
            "issue_date": timezone.now().isoformat(),
        }
        response = self.client.post(url, data, content_type="application/json")

        # Check response status and data
        self.assertEqual(response.status_code, 400)  # 400 Bad Request

    def _test_concurrent_purchase_order_creation(self):
        # Simulate concurrent creation of purchase orders
        url = reverse("purchaseorder-list")
        data1 = {
            "po_number": "PO004",
            "vendor": self.vendor.id,
            "order_date": timezone.now().isoformat(),
            "delivery_date": (timezone.now() + timedelta(days=2)).isoformat(),
            "items": {"item1": "product7", "item2": "product8"},
            "quantity": 20,
            "status": "pending",
            "issue_date": timezone.now().isoformat(),
        }
        data2 = data1.copy()
        data2["po_number"] = "PO005"

        response1 = self.client.post(url, data1, content_type="application/json")
        response2 = self.client.post(url, data2, content_type="application/json")

        # Check response status and data
        self.assertEqual(response1.status_code, 201)  # 201 Created
        self.assertEqual(response2.status_code, 201)  # 201 Created
