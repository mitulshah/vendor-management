# Vendor Management System

This project is a Vendor Management System developed using Django and Django REST Framework (DRF). It provides an API for managing vendors, purchase orders, and calculating vendor performance metrics. This README file provides instructions on how to set up the project, run the server, and run test cases.

## Table of Contents

1. [Project Setup](#project-setup)
    - [Prerequisites](#prerequisites)
    - [Clone the Repository](#clone-the-repository)
    - [Create a Virtual Environment](#create-a-virtual-environment)
    - [Install Dependencies](#install-dependencies)
    - [Run Migrations](#run-migrations)
2. [Running the Server](#running-the-server)
3. [Running Swagger](#running-swagger)
4. [Running Test Cases](#running-test-cases)


## Project Setup

### Prerequisites

- Python 3.8 or higher
- Django 3.2 or higher
- Django REST Framework 3.12 or higher
- Virtual environment (recommended)

### Clone the Repository

Clone the project repository:

```bash
git clone git@gitlab.com:mitulshah/vendor-management.git
cd vendor-management
```

### Create a virtual environment
- python -m venv venv
- Activate the virtual environment:

On Windows:
```bash
    venv\Scripts\activate
```
On macOS/Linux:
```bash
    source venv/bin/activate
```

### Install Dependencies

Install the project dependencies using requirements.txt:

bash
```
    pip install -r requirements.txt
```

### Run Migrations

Run database migrations to set up the database schema:
```bash
    python manage.py migrate
```

### Running the Server
```bash
    python manage.py runserver
```

### Running Swagger
All APIs are available here in swagger page
```bash
    http://localhost:8000/swagger/
```

The server will start on http://127.0.0.1:8000/ by default.

### Running Test Cases
To run the test cases for the project:

Make sure you are in the project directory and the virtual environment is activated.
Run the tests using the following command:
```bash
python manage.py test
```

